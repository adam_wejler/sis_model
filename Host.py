GREEN = (0,  110, 24)
RED	  = (186, 68, 49)

class Host(object):

	allHosts 	  = 0
	infectedHosts = 0
	healthyHosts  = 0

	recoveringToday = 0
	gettingSick	    = 0

	@staticmethod
	def resetStatistics():
		Host.allHosts 	   = 0
		Host.infectedHosts = 0
		Host.healthyHosts  = 0
		Host.recoveringToday = 0
		Host.gettingSick	 = 0

	@staticmethod
	def resetDailyDatas():
		Host.recoveringToday = 0
		Host.gettingSick	 = 0

	def __init__(self, infected, infectionDuration):
		self.infected 		    = infected
		self.dayOfInfection     = 0
		self.infectionDaysLeft  = infectionDuration
		self.infectionDuration	= infectionDuration
		self.hostColor			= GREEN

		self.__updateColor()

		# Static datas updating
		Host.allHosts 			+= 1
		if infected:
			Host.infectedHosts  += 1
		else:
			Host.healthyHosts   += 1

	def getSick(self):
		self.infected 		    = True
		self.dayOfInfection     = 0
		self.__updateColor()

		# Static datas updating
		Host.infectedHosts 		+= 1
		Host.healthyHosts  		-= 1

		Host.gettingSick		+= 1

	def __recover(self):
		self.infected 		    = False
		self.dayOfInfection     = 0
		self.infectionDaysLeft	= self.infectionDuration

		# Static datas updating
		Host.infectedHosts 		-= 1
		Host.healthyHosts  		+= 1

		Host.recoveringToday	+= 1

	def passDay(self):
		if self.infected:
			self.dayOfInfection += 1
			self.infectionDaysLeft = self.infectionDuration - self.dayOfInfection
			if (self.dayOfInfection == self.infectionDuration):
				self.__recover()
		self.__updateColor()

	def __updateColor(self):
		if self.infected:
			self.hostColor 		= RED
		else:
			self.hostColor 		= GREEN

	def getHostColor(self):
		return self.hostColor

	def getHostHelthState(self):
		"""Returns True if infected"""
		return self.infected
