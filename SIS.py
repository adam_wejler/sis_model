from scipy.integrate import odeint			# For integrating equations system
from numpy			 import arange			# For generating list of integers from given range

				#############################################################
				#	t				  - Number of iterations (time)			#
				#	tArray			  - Array of iteration's integers		#
				#	groupsProportions - List containing 'S' and 'I' values	#
				#	beta 			  - Transmission rate					#
				#	gamma			  - Recovery rate 						#
				#############################################################

class SIS(object):

	def __equationsSystem(groupsProportions, tArray, beta, gamma):  
		""" System of differential equations describing SIS model						  """
		""" Output: List containing differentials of 'S' and 'I' in particular iterations """
			
		s = groupsProportions[0]			# S(t)
		i = groupsProportions[1]			# I(t)

		dSdt = gamma*i  - beta*s*i			# dS/dt
		dIdt = beta*s*i - gamma*i			# dI/dt
			
		return [dSdt, dIdt]

	@staticmethod
	def solveEquations(groupsProportions, t, beta, gamma):
		""" Integrating system of differential equations and returning array of 'S' and 'I' for each iteration 't' """

		tArray = arange(0, t+1, 1)			# [0, 1, 2, ... , t-1, t]
		integrationResults = odeint(SIS.__equationsSystem, groupsProportions, tArray, (beta, gamma))

		return integrationResults

if __name__ == '__main__':
	print("\nSIS is for import only. Do not run it as an independent module.")
	input("\nPress ENTER to exit.")