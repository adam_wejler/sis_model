from cx_Freeze import setup, Executable
import os
import sys
os.environ['TCL_LIBRARY'] = "C:\\Users\\Adam\\AppData\\Local\\Programs\\Python\\Python36-32\\tcl\\tcl8.6"
os.environ['TK_LIBRARY'] =  "C:\\Users\\Adam\\AppData\\Local\\Programs\\Python\\Python36-32\\tcl\\tk8.6"

base = None
if sys.platform == "win32":
    base = "Win32GUI"

executables = [Executable(	script="SISmodelSimulator.py", 
							base=base,
							icon="icon.ico"
						)]

setup(

		name = "SIS simulator",
		version = "0.1",
		options = {
					"build_exe": {
						"packages": [
										"pygame",
										"wx",
										"scipy",
										"numpy",
										"os",
										"math",
										"random",
										"matplotlib",
										"tkinter"
									],
						"include_files": ["tcl86t.dll", "tk86t.dll", "icon.ico"]
									}
					},
		executables = executables,
		package_dir={'': ''}
	)