import pygame
import os
from math   import sqrt
from numpy  import zeros, random				# For fast matrix generating, Random choosing
from random import shuffle						# For randomly mixing positions of list elements

from Host import Host

BLACK = (0,     0,   0)
GREEN = (0,   255,   0)
GREEN2= (0,   110,  24)
RED	  = (255,   0,   0)
RED2  = (186,  68,  49)
WHITE = (255, 255, 255)
GREY  = (18,   18,  18)

class Simulator(object):

	sisSimulatorResults = []		# Array with I and S in time taken from simulator (not from counting module directly)

	def __init__(self, 
						population,
						infected,
						infectionPeriod,
						iterationsDelay,
						sisResults,
						windowTitle		= "SIS simulation", 
						fps 			= 60,
						leftMargin 		= 300
				):

		os.environ['SDL_VIDEO_WINDOW_POS'] =  "0, 0"					# Center window initial location
		pygame.init()
		self.myfont = pygame.font.SysFont("impact", 25)					# Font for printing text

		Host.resetStatistics()											# Reset static datas of hosts
		Simulator.sisSimulatorResults = []								# Reset static datas of simulator

		self.infectionPeriod = infectionPeriod							# Time of being sick by each infected individual
		self.initInfect 	 = infected									# Number of initially infected hosts
		self.sisResults		 = sisResults								# I(t) and S(t) list for each iteration
		self.iteration 		 = -1 										# Day of simulation
		self.hosts  		 = self.__placeHostsInArea(population)		# Sharped array of hosts
		self.hosts1Dlist 	 = sum(self.hosts, [])						# Flat 1D list of hosts

		self.leftMarginForOutput = leftMargin 							# Space for output datas

		self.iterationsDelay = iterationsDelay							# Delay of each iteration in miliseconds
		self.fps 	= fps												# Frames Per Second
		self.closed = False												# If program has been closed
		self.clock  = pygame.time.Clock()
		self.simulationFinnished = False

		infoObject	= pygame.display.Info()
		self.displayWidth = infoObject.current_w
		self.displayHeight= infoObject.current_h
		size	= (self.displayWidth, self.displayHeight)


		self.hexSize = 25 												# Hexagons size proportion
		oneHexHeight = 3*self.hexSize+(self.hexSize/3.0)
		botMenuBarHeight = 20
		while(((self.displayHeight-oneHexHeight)/oneHexHeight)-botMenuBarHeight < len(self.hosts)):
			if self.hexSize is 0:
				break
			self.hexSize = self.hexSize - 1
			oneHexHeight = 3*self.hexSize+(self.hexSize/3.0)


		self.screen = pygame.display.set_mode(size, pygame.RESIZABLE)	# Window size=(width, height)
		pygame.display.set_caption(windowTitle)							# Window title
		
		self.__programLoop()

		pygame.quit()

	def __programLoop(self):
		"""Main program loop"""

		while not self.closed:

			if(self.__simulationProcess()):							# Running simulation
				self.iterationsDelay = 0

			for event in pygame.event.get():
				if(event.type == pygame.QUIT):
					self.closed = True
			
			self.clock.tick(self.fps)								# Setting FPS
			
	def __simulationProcess(self):
		"""Return True if simulation finnished"""

		finnished = False											# If simulation is over

		self.iteration += 1
		if(self.iteration < len(self.sisResults)):

			if self.iteration is not 0:
				# Passing new day
				for host in self.hosts1Dlist:
					host.passDay()

				newSickHosts = int(self.sisResults[self.iteration][1] - Host.infectedHosts)
				
				if newSickHosts > 0:
					for host in self.__hostsToInfect(newSickHosts):
						host.getSick()

				infec = []
				infec = self.__getHealthyHosts()

			self.screen.fill(GREY)
			self.__drawMap()										# Updating hosts map
			self.__printDatas()										# Output datas
			self.__drawButton()
			pygame.display.update()
			Host.resetDailyDatas()

			Simulator.sisSimulatorResults.append([Host.healthyHosts, Host.infectedHosts])

			# Animation speed modification
			if not self.iteration == len(self.sisResults)-1:
				previousTime = pygame.time.get_ticks()
				while pygame.time.get_ticks() < previousTime+self.iterationsDelay:
					self.__drawButton()
					pygame.display.update()

					for event in pygame.event.get():
						if(event.type == pygame.QUIT):
							self.closed = True

			if self.iteration == len(self.sisResults)-2:
				self.simulationFinnished = True
		else:
			finnished = True
			self.__drawButton()
			pygame.display.update()
			
		return finnished

	def __getHealthyHosts(self):
		healthyHosts = []
		for host in self.hosts1Dlist:
			if (host.getHostHelthState() == False):
				healthyHosts.append(host)
		return healthyHosts

	def __drawHexagon(self, leftMargin, topMargin, size, color):

		hardShift = self.leftMarginForOutput

		lM = leftMargin
		tM = topMargin
		s  = size

		pygame.draw.polygon(self.screen, color, [[hardShift+lM+2*s, tM+0*s], 
												 [hardShift+lM+4*s, tM+1*s], 
												 [hardShift+lM+4*s, tM+3*s], 
												 [hardShift+lM+2*s, tM+4*s], 
												 [hardShift+lM+0*s, tM+3*s], 
												 [hardShift+lM+0*s, tM+1*s]])

	def __drawMap(self):
		"""Drawing map of hexagons representing hosts: green-healthy, red-infected"""

		size 	  = self.hexSize				# Proportion coeficient of hexagons
		width  	  = 4*size						# Width of hexagon
		height 	  = width 						# Height of hexagon
		space     = size/3 						# Distance between hexagons
		leftShift = False 						# Row put to left in the current line drawing

		i = 0
		for row in self.hosts:
			i += 1
			j = 0
			for host in row:
				j += 1

				xShift = j*(width+space)
				yShift = i*(0.75*height+space)

				if leftShift:
					xShift -= (width+space)/4
				else:
					xShift += (width+space)/4

				self.__drawHexagon(xShift, yShift, size, host.getHostColor())

			leftShift = not leftShift

	def __arrayOfHosts(self, population):
		"""Sharped array that will contain hosts"""
		floatSqrtPopulation = sqrt(population)
		length 			  = int(floatSqrtPopulation)
		restOfPopulation  = population - length**2

		if restOfPopulation is 0:
			area = zeros((length, length)).tolist()
		else:
			x = 0
			if (restOfPopulation % length == 0):
				x = int(restOfPopulation / length)
			else:
				x = int(restOfPopulation / length) + 1

			area = zeros((length+x, length)).tolist()

			if not ((restOfPopulation%length) == 0):
				for i in range(length - (population % length)):
					del area[-1][-1]
		return area

	def __placeHostsInArea(self, population):
		"""Placing true hosts into sharped array imitating map"""
		area = self.__arrayOfHosts(population)

		i = -1
		for row in area:
			i += 1
			j = -1
			for col in row:
				j += 1
				area[i][j] = Host(False, self.infectionPeriod)

		# Initial infecting hosts (randomly selected)
		hosts1Dlist = sum(area, [])
		toInfect 	= random.choice(hosts1Dlist, self.initInfect, replace=False)
		for host in toInfect:
			host.getSick()

		return area

	def __hostsToInfect(self, amount):
		"""Returns list of hosts that may become infected"""
		hosts1Dlist = sum(self.hosts, [])
		shuffle(hosts1Dlist)
		hostsToInfect = []

		for host in hosts1Dlist:
			if not host.getHostHelthState():
				# If not infected
				hostsToInfect.append(host)

		return hostsToInfect[:amount]

	def __printDatas(self):
		spaceX = 80
		spaceY = 30
		shiftY = 60

		iteration = "Iteracja:     "+str(self.iteration)
		label1 = self.myfont.render(iteration, 1, WHITE)
		self.screen.blit(label1, (spaceX, spaceY+shiftY))

		healthyHosts = "Zdrowi:     "+str(Host.healthyHosts)
		label2 = self.myfont.render(healthyHosts, 1, GREEN2)
		self.screen.blit(label2, (spaceX, 3*spaceY+shiftY))

		sickHosts = "Chorzy:     "+str(Host.infectedHosts)
		label3 = self.myfont.render(sickHosts, 1, RED)
		self.screen.blit(label3, (spaceX, 4*spaceY+shiftY))

		recovering = "Zdrowiejący:     "+str(Host.recoveringToday)
		label4 = self.myfont.render(recovering, 1, GREEN2)
		self.screen.blit(label4, (spaceX, 6*spaceY+shiftY))

		gettingSick = "Zarażani:     "+str(Host.gettingSick)
		label5 = self.myfont.render(gettingSick, 1, RED)
		self.screen.blit(label5, (spaceX, 7*spaceY+shiftY))

		if self.simulationFinnished:
			finnishedInfo = "SYMULACJA ZAKOŃCZONA"
			label6 = self.myfont.render(finnishedInfo, 1, GREEN)
			self.screen.blit(label6, (25, 30))

			line = "__________________"
			label7 = self.myfont.render(line, 1, GREEN)
			self.screen.blit(label7, (23, 40))

	def __drawButton(self):
		mouse = pygame.mouse.get_pos()
		click = pygame.mouse.get_pressed()

		if 250 > mouse[0] > 50 and 430 > mouse[1] > 380:
			pygame.draw.rect(self.screen, RED, (50, 380, 200, 50))
			if click[0] is 1:
				self.closed = True
		else:
			pygame.draw.rect(self.screen, RED2, (50, 380, 200, 50))
		
		label = self.myfont.render("Zamknij", 1, WHITE)
		self.screen.blit(label, (110, 388))