from SIS import SIS
import pylab as pl

#TEST

I0	  = 1e-6
INPUT = (1.0-I0, I0)
ND	  = 70
beta  = 0.4247
gamma = 0.14286
RES=SIS.solveEquations(INPUT, ND, beta, gamma)
print(RES)

pl.subplot(211)
pl.plot(RES[:,0], '-g', label='Susceptibles')
pl.title('Program_2_5.py')
pl.xlabel('Time')
pl.ylabel('Susceptibles')
pl.subplot(212)
pl.plot(RES[:,1], '-r', label='Infectious')
pl.xlabel('Time')
pl.ylabel('Infectious')
pl.show()