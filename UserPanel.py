import wx
import numpy as np
from Simulator import Simulator
from SIS 	   import SIS

# Plotting
import matplotlib
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas

import pylab as pl

GREY = (0.224, 0.224, 0.224)

class UserPanel(wx.Frame):

	def __init__(self, parent, windowTitle="Model SIS simulator", size=(1100, 700)):
		super(UserPanel, self).__init__(parent, title=windowTitle, size=size)

		icon = wx.Icon()
		icon.CopyFromBitmap(wx.Bitmap("icon.ico", wx.BITMAP_TYPE_ANY))
		self.SetIcon(icon)

		self.activated = False

		self.topPanel = wx.Panel(self)

		self.rightPanel = wx.Panel(self.topPanel, -1, pos=(650, 0), size=(550, 700))

		# Right panel content

			# Texts

		fontBig = wx.Font(25, wx.DEFAULT, wx.NORMAL, wx.NORMAL)

		infoText = wx.StaticText(self.rightPanel, -1, "Dane wejściowe", (50, 10))
		infoText.SetFont(fontBig)

		fontNormal = wx.Font(15, wx.DEFAULT, wx.NORMAL, wx.NORMAL)

		topMargin = 40

		textPopulation = wx.StaticText(self.rightPanel, -1, "Populacja:", (10, 60))
		textPopulation.SetFont(fontNormal)

		textInfectionTime = wx.StaticText(self.rightPanel, -1, "Czas infekcji [1/"+u'\u03B3'+"]:", (10, 60+topMargin))
		textInfectionTime.SetFont(fontNormal)

		textTransmissionRate = wx.StaticText(self.rightPanel, -1, "Współczynnik zachorowań ["+u'\u03B2'+"]:", (10, 60+2*topMargin))
		textTransmissionRate.SetFont(fontNormal)

		textInitialSick = wx.StaticText(self.rightPanel, -1, "Chorzy [I(0)]:", (10, 60+3*topMargin))
		textInitialSick.SetFont(fontNormal)

		infoText2 = wx.StaticText(self.rightPanel, -1, "Ustawienia symulatora", (25, 300))
		infoText2.SetFont(fontBig)

		textSpeed = wx.StaticText(self.rightPanel, -1, "Czas iteracji [milisekundy]:", (10, 120+6*topMargin))
		textSpeed.SetFont(fontNormal)

		textDays = wx.StaticText(self.rightPanel, -1, "Dni [iteracje]:", (10, 400))
		textDays.SetFont(fontNormal)

			# Inputs

		self.inputPopulation = wx.TextCtrl(self.rightPanel, -1, "4000", size=(100, 30), pos=(110, 58))
		self.inputPopulation.SetFont(fontNormal)

		self.inputInfectionTime = wx.TextCtrl(self.rightPanel, -1, "2", size=(100, 30), pos=(180, 58+topMargin))
		self.inputInfectionTime.SetFont(fontNormal)

		self.inputTransmissionRate = wx.TextCtrl(self.rightPanel, -1, "0.0003247", size=(150, 30), pos=(280, 58+2*topMargin))
		self.inputTransmissionRate.SetFont(fontNormal)

		self.inputInitialSick = wx.TextCtrl(self.rightPanel, -1, "1", size=(100, 30), pos=(140, 58+3*topMargin))
		self.inputInitialSick.SetFont(fontNormal)

		self.inputSpeed = wx.TextCtrl(self.rightPanel, -1, "300", size=(100, 30), pos=(250, 355))
		self.inputSpeed.SetFont(fontNormal)

		self.inputDays = wx.TextCtrl(self.rightPanel, -1, "50", size=(100, 30), pos=(140, 400))
		self.inputDays.SetFont(fontNormal)

			# Submit button

		submitButton = wx.Button(self.rightPanel, -1, label='Start', size=(400, 60), pos=(17, 500))
		submitButton.SetFont(fontNormal)
		self.submitButton = submitButton
		self.submitButton.Bind(wx.EVT_BUTTON, self.__clicked)

		# Status bar
		self.statusBar = self.CreateStatusBar()
		self.statusBar.SetStatusText("Wprowadź dane i wciśnij przycisk START aby uruchomić symulator i wyświetlić wykres.")

		self.Centre()
		self.Show()

	def __numpyToIntArray(self, numpyInput):
		numpyArray = numpyInput
		for each in numpyArray:
			np.around(each)

		number_cols = len(numpyArray[0])
		number_rows = len(numpyArray)

		intArray = [[0] * number_cols for i in range(number_rows)]	# Empty 2D array

		for i in range(number_rows):
			for j in range(number_cols):
				intArray[i][j]=int(round(numpyArray[i][j]))

		return intArray
	def __sumOfDatasEnsurance(self, population, resultsArray):
		"""For keeping proper integer numbers of S(t) after type conversion"""
		afterControll = resultsArray
		for row in afterControll:
			row[0] = population - row[1]

		return afterControll
	def __clicked(self, event):

		self.activated = not (self.activated)

		if(self.activated):
			self.submitButton.Disable()

		inputList = self.__getInputs()

		if inputList is not False: 

			# Inputs:
			population		= inputList[0]
			i0	  			= inputList[3]
			inits 			= (population-i0, i0)
			t	  			= inputList[4]
			beta  			= inputList[2]
			infectionPeriod = inputList[1]
			gamma 			= 1.0/infectionPeriod
			delay 			= inputList[5]

			# Computing SIS model
			sisResults = SIS.solveEquations(inits, t, beta, gamma)
			
			intResults = self.__numpyToIntArray(sisResults)
			intResults = self.__sumOfDatasEnsurance(population, intResults)

			# Creating simulator window
			Simulator(	population 		= population, 
						infected 		= i0, 
						infectionPeriod = infectionPeriod,
						iterationsDelay = delay,
						sisResults 		= intResults,
					 )

			# Plot updating
			self.leftPanel = MatplotPanel(self.topPanel, Simulator.sisSimulatorResults)

		self.activated = False
		self.submitButton.Enable()

	def __getInputs(self):
		"""If proper inputs then returns list of validated int or float datas. Else returns False"""

		# Inputs validation

		str_population 		   = self.inputPopulation.GetValue()
		str_infectionTime      = self.inputInfectionTime.GetValue()
		str_transmissionRate   = self.inputTransmissionRate.GetValue()
		str_initialSick 	   = self.inputInitialSick.GetValue()
		str_days 			   = self.inputDays.GetValue()
		str_speed 			   = self.inputSpeed.GetValue()

		isOK_population 	   = self.__representsInt(str_population)
		isOK_infectionTime 	   = self.__representsInt(str_infectionTime)
		isOK_transmissionRate  = self.__representsFloat(str_transmissionRate)
		isOK_initialSick 	   = self.__representsInt(str_initialSick)
		isOK_days 			   = self.__representsInt(str_days)
		isOK_speed 			   = self.__representsInt(str_speed)

		int_population 		   = False
		int_infectionTime 	   = False
		float_transmissionRate = False
		int_initialSick 	   = False
		int_days 			   = False
		int_speed 			   = False

		isPositive_population 		= False
		isPositive_infectionTime 	= False
		isPositive_transmissionRate = False
		isPositive_initialSick 		= False
		isPositive_days 			= False
		isPositive_speed 			= False

		if isOK_population:
			int_population = int(str_population)
			isPositive_population = self.__isPositive(int_population)
		if isOK_infectionTime:
			int_infectionTime = int(str_infectionTime)
			isPositive_infectionTime = self.__isPositive(int_infectionTime)
		if isOK_transmissionRate:
			float_transmissionRate = float(str_transmissionRate)
			isPositive_transmissionRate = self.__isPositive(float_transmissionRate)
		if isOK_initialSick:
			int_initialSick = int(str_initialSick)
			isPositive_initialSick = self.__isPositive(int_initialSick)
		if isOK_days:
			int_days = int(str_days)
			isPositive_days = self.__isPositive(int_days)
		if isOK_speed:
			int_speed = int(str_speed)
			isPositive_speed = self.__isPositive(int_speed)

		populationTooBig  = False
		initialSickTooBig = False

		infectionPeriodTooBig = False
		daysTooBig = False
		iteracionsTooBig = False

		transmissionRateTooBig = False

		if isPositive_population:
			if int_population > 10000:
				populationTooBig = True
		if isPositive_initialSick:
			if int_initialSick > int_population:
				initialSickTooBig = True
		
		if isPositive_infectionTime:
			if int_infectionTime > 10000:
				infectionPeriodTooBig = True
		if isPositive_days:
			if int_days > 10000:
				daysTooBig = True
		if isPositive_speed:
			if int_speed > 3000:
				iteracionsTooBig = True

		if isPositive_transmissionRate:
			if float_transmissionRate > 1.0:
				transmissionRateTooBig = True

		informationWillAppear = False
		if (
			(not isPositive_population)		  or
			(not isPositive_infectionTime) 	  or
			(not isPositive_transmissionRate) or
			(not isPositive_initialSick)	  or
			(not isPositive_days) 			  or
			(not isPositive_speed) 			  or
			populationTooBig 				  or
			initialSickTooBig 				  or
			infectionPeriodTooBig 			  or
			daysTooBig						  or
			iteracionsTooBig 				  or
			transmissionRateTooBig
			):

			informationWillAppear = True

		infoBoxContent = []
		if not isPositive_population:
			infoBoxContent.append("\nPopulacja powinna być dodatnią liczbą całkowitą.")
		if not isPositive_infectionTime:
			infoBoxContent.append("\nCzas infekcji musi być dodatnią liczbą całkowitą.")
		if not isPositive_transmissionRate:
			infoBoxContent.append("\nWspółczynnik zachorowań musi być dodatnią liczbą rzeczywistą.")
		if not isPositive_initialSick:
			infoBoxContent.append("\nPoczątkowa liczba chorych musi być całkowitą liczbą dodatnią.")
		if not isPositive_days:
			infoBoxContent.append("\nLiczba iteracji musi być dodatnią liczbą całkowitą.")
		if not isPositive_speed:
			infoBoxContent.append("\nCzas trwania symulacji iteracji musi być dodatnią liczbą całkowitą.")
		if populationTooBig:
			infoBoxContent.append("\nPopulacja powinna być mniejsza niż 10.000 osobników.")
		if initialSickTooBig:
			infoBoxContent.append("\nLiczba chorych nie może być większa od łącznej liczby osobników.")
		if infectionPeriodTooBig:
			infoBoxContent.append("\nMaksymalny czas trwania infekcji to 10.000 dni.")
		if daysTooBig:
			infoBoxContent.append("\nDługość trwania symulacji nie może przekraczać 10.000 dni.")
		if iteracionsTooBig:
			infoBoxContent.append("\nCzas wyświetlania przypadający na każdą iterazję nie może być dłuższy niż 3 sekundy.")
		if transmissionRateTooBig:
			infoBoxContent.append("\nWspółczynnik zachorowań nie może być większy od 1.")

		infoTextInBox = ""
		for each in infoBoxContent:
			infoTextInBox += each

		# Warning alert
		if informationWillAppear:
			dlg = wx.MessageDialog(None, infoTextInBox, "Błędne dane!", wx.OK | wx.ICON_WARNING)
			dlg.ShowModal()
			dlg.Destroy()

		output = False
		if not informationWillAppear:
			output = [
						int_population, 
						int_infectionTime, 
						float_transmissionRate, 
						int_initialSick, 
						int_days, 
						int_speed,
					 ]

		return output

	def __representsInt(self, string):
		try:
			int(string)
			return True
		except ValueError:
			 return False
	def __representsFloat(self, string):
		try:
			float(string)
			return True
		except ValueError:
			 return False
	def __isPositive(self, number):
		if number > 0:
			return True
		else:
			return False

####################################################
# Class of object creating panel for printing plot #
####################################################

class MatplotPanel(wx.Panel):
	def __init__(self, parent, sisResults):
		wx.Panel.__init__(self, parent,-1, pos=(0, 0), size=(650, 700))

		self.figure = Figure()
		self.figure.set_size_inches(7.15, 6.6)
		self.axes = self.figure.add_subplot(111)

		self.axes.set_title('Liczebność chorych i zdrowych jednostek w czasie')
		self.axes.set_xlabel('Numer iteracji')
		self.axes.set_ylabel('Liczebność osobników')
		self.axes.grid(color=GREY, linestyle='-', linewidth=0.125)

		x = np.arange(0.0, len(sisResults), 1.0)
		y = []
		z = []

		for each in sisResults:
			y.append(each[1])
			z.append(each[0])

		l1, l2 = self.axes.plot(x, y, 'r.-', x, z, 'g.-')

		self.figure.legend((l1, l2), ('Osobniki zainfekowane', 'Osobniki zdrowe'), 'upper left')

		self.canvas = FigureCanvas(self,-1,self.figure)
